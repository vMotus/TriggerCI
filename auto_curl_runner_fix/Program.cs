﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace auto_curl_runner_fix
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //Get branches
            //Check pull for new commit
            //If new commit trigger build for branch

            foreach(string branch in getBranches())
            {
                if(checkModified(branch))
                {
                    trigger(branch);
                }
            }
        }

        private static bool checkModified(string branch)
        {
            run("git.exe", "checkout " + branch);
            string pullOut = run("git.exe", "pull");
            return !pullOut.Contains("up-to-date");
        }

        private static List<string> getBranches()
        {
            List<string> ret = new List<string>();
            run("git.exe", "fetch");
            string remoteBranchStr = run("git.exe", "branch -r");
            foreach(var line in remoteBranchStr.Split(new string[] { Environment.NewLine }, new StringSplitOptions()).Select(x => x.Substring("  origin/".Length)))
            {
                int i = line.IndexOf(" ->");
                if (i != -1)
                {
                    ret.Add(line.Substring(0, i));
                }
                else
                    ret.Add(line);
            }
            return ret;
        }

        private static void trigger(string branch)
        {
            /*  curl -X POST \
                -F token = TOKEN \
                -F ref=REF_NAME \
                https://git.science.uu.nl/api/v3/projects/184/trigger/builds
            */
            run("curl.exe", "-X POST -F token=f0c0b62cd603ce935d36cfbd8ccd25 -F ref=" + branch + " https://git.science.uu.nl/api/v3/projects/184/trigger/builds");
        }

        private static string run(string cmd, string args)
        {
            Console.WriteLine("cmd: " + cmd);
            Console.WriteLine("args: " + args);
            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.Arguments = "/C " + cmd + " " + args;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.UseShellExecute = false;
            proc.Start();

            /*git.StandardInput.WriteLine("echo Oscar");
            git.StandardInput.Flush();
            git.StandardInput.Close();
            git.WaitForExit();*/
            string output = proc.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
            return output;
        }
    }
}
